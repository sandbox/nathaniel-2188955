/**
 * @file
 * Home page .photos-grid hover effect.
 */

(function ($) {
  'use strict';

  $(function () {
    $('.photos-grid .views-col .views-field-cover-id').mouseenter(function () {
      $(this).addClass('photos-hover');
      $('.photos-title', this).stop().animate({
        'bottom' : '5px',
        'opacity' : '1'
      }, 500);
    }).mouseleave(function () {
      $(this).removeClass('photos-hover');
      $('.photos-title', this).stop().animate({
        'bottom' : '-25px',
        'opacity' : '0'
      }, 500);
    });
    $('.photos-img-overlay').click(function () {
      location.href = $(this).parents('.views-col').find('.field-content a').attr('href');
    });
  });

})(jQuery);
